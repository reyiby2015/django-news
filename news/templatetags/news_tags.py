from django import template
from django.db.models import Count, F
from django.core.cache import cache

from news.models import Category

register = template.Library()


@register.simple_tag()
def get_categories():
    """Simple tag - для демонстрации, используем Inclusion tag"""
    return Category.objects.all()


@register.inclusion_tag('news/list_categories.html')
def show_categories():
    """Inclusion tag"""
    categories = Category.objects.annotate(
        cnt=Count('news', filter=F('news__is_published'))).filter(cnt__gt=0)
    # Использование API низкого уровня кэширования
    # categories = cache.get('categories')
    # if not categories:
    #     categories = Category.objects.annotate(
    #         cnt=Count('news', filter=F('news__is_published'))).filter(
    #             cnt__gt=0)
    #     cache.set('categories', categories, 30)

    # categories = Category.objects.annotate(
    #     cnt=Count('news')).filter(cnt__gt=0)
    # categories = Category.objects.all()
    return {'categories': categories}
